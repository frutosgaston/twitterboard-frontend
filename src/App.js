import React, { Component } from 'react';
import './App.css';
import InterestList from './components/InterestList';
import Dashboard from './components/Dashboard';
import store from './redux/store';
import { Provider } from 'react-redux';

class App extends Component {
  render() {
    return (
      <Provider store={ store }>
        <div>
          <InterestList interests={[]}/>
          <Dashboard/>
        </div>
      </Provider>
    );
  }
}

export default App;
