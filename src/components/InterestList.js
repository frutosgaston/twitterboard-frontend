import React, { Component } from 'react'
import '../styles/InterestList.css'
import InterestItem from './InterestItem'
import { connect } from 'react-redux'
import AuthenticationButtons from './AuthenticationButtons'
import { update } from '../redux/actions/users'

class InterestList extends Component {

    constructor(props) {
        super(props);

        this.dispatch = this.props.dispatch;

        this.state = { newInterest: '' };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ newInterest: event.target.value });
    }

    handleSubmit(event) {
        var currentUser = this.props.users.current.user
        var user = { username: currentUser.username, interests: currentUser.interests.concat([this.state.newInterest]) }
        this.dispatch(update(user))
        event.preventDefault();
    }

    render(){
        var current = this.props.users.current
        return(
            <div className={'interest-list'}>
                <AuthenticationButtons/>
                <div>{
                    current &&
                    <div>
                        <h2 className={'interests-title'}>Your interests:</h2>
                        <div>{
                            current.user.interests.map(
                                (interest) => (    
                                    <InterestItem key={interest} interest={interest} />
                                )
                            )
                        }
                        </div>

                        <form className={'new-interest-form'} onSubmit={this.handleSubmit}>
                            <div className="input-effect">
                                <input className="new-interest-input" type="text" placeholder="" onChange={this.handleChange}/>
                                <label className={'new-interest-label'}>New Interest</label>
                                <span className={"focus-border"}></span>
                            </div>
                            <input type="submit" value="Submit"/>
                        </form>
                    </div>
                }
                </div>
            </div>
        )
}


}

const mapStateToProps = (state) =>({
    ...state,
    user :  state.user
})

export default connect(mapStateToProps) (InterestList);