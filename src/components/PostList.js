import React, { Component } from 'react'
import '../styles/Post.css'
import Post from './Post'

class PostList extends Component {

	render() {
		return(
			<div className={'posts-container'}>{
				this.props.posts.map(
                    (post) => (
                    	<Post key={post.pageId} post={post} />
                    )
                )
			}
			</div>
		)
	}

}

export default PostList