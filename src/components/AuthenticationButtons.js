import React, { Component } from 'react'
import '../styles/InterestList.css'
import { connect } from 'react-redux'
import { login, signup, logout } from '../redux/actions/users'

class AuthenticationButtons extends Component {

    constructor(props){
        super(props);

        this.dispatch = this.props.dispatch;
    }

    loginClick() { 
        this.dispatch(login("gaston"))
    }

    signupClick() { 
        this.dispatch(signup("gaston"))
    }

    logoutClick() {
    	this.dispatch(logout())
    }

    render(){
        const currentUser = this.props.users.current
        return(
            <div>{ 
                currentUser ? (
                	<div>
	                	<div className={'logged-user'}>{'@' + currentUser.user.username}</div>
	                    <div className={'auth-button logout'} onClick={(e) => this.logoutClick(e)}>{'Log Out'}</div>
                    </div>
                ) : (
                    <div>
                        <div className={'auth-button login'} onClick={(e) => this.loginClick(e)}>{'Log In'}</div>
                        <div className={'auth-button signup'} onClick={(e) => this.signupClick(e)}>{'Sign Up'}</div>
                    </div>
                )
	            }
            </div>
        )
    }
}

const mapStateToProps = (state) =>({
    ...state,
    user :  state.user
})


export default connect(mapStateToProps) (AuthenticationButtons);