import React, { Component } from 'react'
import '../styles/Post.css'
import TimeAgo from 'javascript-time-ago'
import es from 'javascript-time-ago/locale/es'

TimeAgo.locale(es)

const getTimeAgo = (date) => new TimeAgo('es-AR').format(date);

class Post extends Component {

	formatColor(ary) {
    	return 'rgb(' + ary.join(', ') + ')';
  	}

	applyColor() {
	    return this.formatColor(this.chooseColor());
	}

	chooseColor() {
		var minColorNumber = 200;
		for (var i = 0, random = []; i < 3; i++) {
  			random.push(Math.floor(Math.random() * (256 - minColorNumber) + minColorNumber));
		}
		return random; 
	}

	randomWidth() { return Math.floor(Math.random() * (40 - 30) + 30) + '%' }

	render() {
		var post = this.props.post
		return(
				<div className={'post-box'} style={{ backgroundColor: this.applyColor(), flexBasis: this.randomWidth() }}>
					<a className={'post-title post-href'} href={"https://twitter.com/anything/status/" + post.pageId} target={'_blank'}>
						<h3 className={'post-writer'}>{post.writerName}</h3>
						<div className={'post-user'}>{'@' + post.writerUser}</div>
					</a>
					<a className={'created-time post-href'} href={"https://twitter.com/anything/status/" + post.pageId} target={'_blank'}>
						{getTimeAgo(post.createdAt)}
					</a>
					<p className={'post-message'}>{post.message}</p>
				</div>
		)
	}

}

export default Post