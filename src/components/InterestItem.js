import React, { Component } from 'react'
import '../styles/InterestList.css'
import { connect } from 'react-redux'
import { getPosts } from '../redux/actions/posts'

class InterestItem extends Component {

    constructor(props){
        super(props);

        this.dispatch = this.props.dispatch;
    }

    interestClick() { 
        this.dispatch(getPosts(this.props.interest))
    }

    render() {
    	return(
	    <div>
	        <div onClick={ (e) => this.interestClick(e) } className={'interest-item-line'}>
	            <div className={'interest-item'}>
	                <div title={this.props.interest}className={'interest'}>
	                    {this.props.interest}
	                </div>
	            </div>
	        </div> 
	    </div>
	    )
	}
}

const mapStateToProps = (state) =>({
    ...state,
    posts:  state.posts
})

export default connect(mapStateToProps) (InterestItem);