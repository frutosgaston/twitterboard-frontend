import React, { Component } from 'react'
import { connect } from 'react-redux'
import PostList from './PostList'

class Dashboard extends Component {

	render() {
		return(
			<div className={'dashboard'}>{
				this.props.posts && this.props.posts.all && (
					<PostList posts={this.props.posts.all.posts}/>
				)
			}
			</div>
		)
	}

}

const mapStateToProps = (state) =>({
    ...state,
    posts:  state.posts
})


export default connect(mapStateToProps) (Dashboard);