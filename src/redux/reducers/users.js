const initialState = {
  current: null,
  status: 'INIT'
}

const actionHandlers = {
  "LOADING": (action) => ({}),
  "LOGGED_IN": (action) => ({
    current: action.payload
  }),
  "SIGNED_UP": (action) => ({
    current: action.payload
  }),
  "FAILED_ON_LOG_IN": (action) => ({
    all: null
  }),
  "FAILED_ON_SIGN_UP": (action) => ({
    all: null
  }),
  "LOGOUT": (action) => ({
    current: null
  }),
  "UPDATED": (action) => ({
    current: action.payload
  })
}

export default (state = initialState, action = {}) => {
  
  const handler = actionHandlers[action.type];

  const newState = handler ? Object.assign(
    {},
    state,
    handler(action),
    {
      status: action.type
    }
  )
    : state;

  return newState;
}