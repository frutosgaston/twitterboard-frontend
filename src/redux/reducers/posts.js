const initialState = {
  all: null,
  status: 'INIT'
}

const actionHandlers = {
  "LOADING": (action) => ({}),
  "POSTS": (action) => ({ all: action.payload }),
  "FAILED_ON_GET_POSTS": (action) => ({ all: null })
}

export default (state = initialState, action = {}) => {
  
  const handler = actionHandlers[action.type];

  const newState = handler ? Object.assign(
    {},
    state,
    handler(action),
    {
      status: action.type
    }
  )
    : state;

  return newState;
}