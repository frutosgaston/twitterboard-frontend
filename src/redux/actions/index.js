import { update, logout, signup, login } from './users'
import { getPosts } from './posts'

export { update, logout, signup, login, posts }