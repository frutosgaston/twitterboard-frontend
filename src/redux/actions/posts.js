import url from 'url';
import axios from 'axios';

export function getPosts(interest) {
  return async (dispatch, getState) => {
    dispatch({ type: 'LOADING', payload: {} });
    axios
      .get(url.resolve((process.env.REACT_APP_API_HOST || 'http://localhost:8080'), '/posts?q=' + interest))
      .then(function (response) {
        dispatch({ type: 'POSTS', payload: { 
        	posts: Object.assign(response.data.map((post) => Object.assign(post, {createdAt: new Date(post.createdAt)}))) 
    	} });
      })
      .catch(function (error) {
        dispatch({ type: 'FAILED_ON_GET_POSTS', payload: {} });
      })
  }
}