import url from 'url';
import axios from 'axios';

export function login(username) {
  return async (dispatch, getState) => {
    dispatch({ type: 'LOADING', payload: {} });
    axios
      .get(url.resolve((process.env.REACT_APP_API_HOST || 'http://localhost:8080'), '/user/' + username))
      .then(function (response) {
        dispatch({ type: 'LOGGED_IN', payload: { user: Object.assign(response.data) } });
      })
      .catch(function (error) {
        dispatch({ type: 'FAILED_ON_LOG_IN', payload: {} });
      })
  }
}

export function signup(username) {
  return async (dispatch, getState) => {
    dispatch({ type: 'LOADING', payload: {} });
    axios
      .post(url.resolve((process.env.REACT_APP_API_HOST || 'http://localhost:8080'), '/user/'), { username: username, interests: []})
      .then(function (response) {
        dispatch({ type: 'SIGNED_UP', payload: { user: Object.assign(response.data) } });
      })
      .catch(function (error) {
        dispatch({ type: 'FAILED_ON_SIGN_UP', payload: {} });
      })
  }
}

export function logout() {
  return async (dispatch, getState) => {
    dispatch({ type: 'LOGOUT', payload: {} });
  }
}

export function update(user) {
  return async (dispatch, getState) => {
    dispatch({ type: 'LOADING', payload: {} });
    axios
      .patch(url.resolve((process.env.REACT_APP_API_HOST || 'http://localhost:8080'), '/user/' + user.username), user)
      .then(function (response) {
        dispatch({ type: 'UPDATED', payload: { user: Object.assign(response.data) } });
      })
      .catch(function (error) {
        dispatch({ type: 'FAILED_ON_UPDATE', payload: {} });
      })
  }
}